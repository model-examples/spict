# Продукционная модель SPiCT
> English description available above

SPiCT - стохастическая продукционная модель (stochastic Surplus Production model In Continuous Time), реализованная на основе аппроксимации продукционной кривой Флэтчера (dome-shaped Fletcher production curve).

## 1. Входные данные
В качестве входных данных необходимо заполнить данные о многолетнем вылове (C) и улове на единицу усилия (CPUE) в файл `/input/data.csv`. Разделитель десятичной части - точка.

## 2. Параметризация модели
Параметризация модели выполняется в начале файла `Run.R`:

```R
config.analys.name <- "Black sea turbot"
config.spict.robust = FALSE # use robust procedure ignore extremal catches? CAUTION: robustness BROKE forecasting!
config.spict.retro <- 3 # retrospective years horizon
config.catch.units <- " t" # set units for catch values
```

## 3. Установка зависимостей
Установка зависимостей и нужных пакетов может быть выполнена при помощи исполнения целиком файла `Install.R`:

```R
source("Install.R")
```

## 4. Построение модели
Построение модели и выполнение всех этапов выполняется при помощи исполнения файла целиком: `Run.R`:

```R
source("Run.R")
```

## 5. Результаты
После подгонки модели результаты будут сохранены в виде отчета в файле `Report.html`.

## 6. Источники

  - [DTUAqua/spict](https://github.com/DTUAqua/spict)
  - Pedersen, M. W. and Berg, C. W. (2017), A stochastic surplus production model in continuous time. Fish and Fisheries,
  18(2): 226-243. doi:10.1111/faf.12174
  
## 7. Автор

Mikhail Piatinskii, Azov-Black sea branch of VNIRO 

Email: pyatinskiy_m_m@azniirkh.ru. Telegram: [@zenn1989](https://t.me/zenn1989). Twitter: [@followzenn](https://twitter.com/followzenn)

# SPiCT production model

SPICT - stochastic Surplus Production model In Continuous Time, which in addition to stock dynamics also models the dynamics of the fisheries

## 1. Input data
Input data, such as total landings, C, and catch per unit effort, CPUE, should be done in `/input/data.csv`.

## 2. Parametrization
Model parametrization available in config section of `Run.R`:

```R
config.analys.name <- "Black sea turbot"
config.spict.robust = FALSE # use robust procedure ignore extremal catches? CAUTION: robustness BROKE forecasting!
config.spict.retro <- 3 # retrospective years horizon
config.catch.units <- " t" # set units for catch values
```

## 3. Dependencies
All dependencies can be installed automaticly by processing `Install.R`:

```R
source("Install.R")
```

## 4. Fit model
Model can be automaticly done by calling `Run.R`:

```R
source("Run.R")
```

## 5. Results
All results are sumarized in `Report.html`.

## 6. Credentials

  - [DTUAqua/spict](https://github.com/DTUAqua/spict)
  - Pedersen, M. W. and Berg, C. W. (2017), A stochastic surplus production model in continuous time. Fish and Fisheries,
  18(2): 226-243. doi:10.1111/faf.12174
  
## 7. Author

Mikhail Piatinskii, Azov-Black sea branch of VNIRO 

Email: pyatinskiy_m_m@azniirkh.ru. Telegram: [@zenn1989](https://t.me/zenn1989). Twitter: [@followzenn](https://twitter.com/followzenn)